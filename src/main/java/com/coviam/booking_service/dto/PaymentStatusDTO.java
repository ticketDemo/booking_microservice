package com.coviam.booking_service.dto;

import javax.validation.constraints.NotNull;
import java.util.StringJoiner;


public class PaymentStatusDTO {

    String bookingId;
    @NotNull
    String paymentId;
    @NotNull
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("bookingId = " + bookingId)
                .add("paymentId = " + paymentId)
                .add("status = " + status)
                .toString();
    }
}
