package com.coviam.booking_service.dto;


import java.util.StringJoiner;

public class PassengerDTO {

    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }



    public PassengerDTO(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public PassengerDTO() {

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("age = " + age)
                .add("name = " + name)
                .toString();
    }
}
