package com.coviam.booking_service.dto;

import com.coviam.booking_service.entity.Booking;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;


public class BookingDTO {

    private String id;
    private String userId;
    private String superPnr;
    private String bookingType;
    private String bookingSource;
    private Booking.Status bookingStatus;
    private Booking.Status paymentStatus;
    private String phoneNumber;
    private String bookingEmail;
    private List<PassengerDTO> passengers;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBookingType() {
        return bookingType;
    }

    public void setBookingType(String bookingType) {
        this.bookingType = bookingType;
    }

    public String getBookingSource() {
        return bookingSource;
    }

    public void setBookingSource(String bookingSource) {
        this.bookingSource = bookingSource;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBookingEmail() {
        return bookingEmail;
    }

    public void setBookingEmail(String bookingEmail) {
        this.bookingEmail = bookingEmail;
    }

    public String getSuperPnr() {
        return superPnr;
    }

    public void setSuperPnr(String superPnr) {
        this.superPnr = superPnr;
    }

    public List<PassengerDTO> getPassengers() {
        return passengers;
    }


    public void setPassengers(List<PassengerDTO> passengers) {
        this.passengers = passengers;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Booking.Status getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Booking.Status bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Booking.Status getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(Booking.Status paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public BookingDTO(String userId, String bookingType, String bookingSource, String phoneNumber, String bookingEmail, String superPnr, List<PassengerDTO> passengers) {
        this.userId = userId;
        this.bookingType = bookingType;
        this.bookingSource = bookingSource;
        this.phoneNumber = phoneNumber;
        this.bookingEmail = bookingEmail;
        this.superPnr = superPnr;
        this.passengers = passengers;
    }

    public BookingDTO() {

    }


    public BookingDTO inflateFromBooking(Booking booking) {
        BeanUtils.copyProperties(booking, this);
        setPassengers(new ArrayList<>());
        booking.getPassengers().forEach(passenger -> {
            PassengerDTO passengerDTO = new PassengerDTO();
            BeanUtils.copyProperties(passenger, passengerDTO);
            this.getPassengers().add(passengerDTO);
        });
        return this;
    }


    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("id = " + id)
                .add("userId = " + userId)
                .add("bookingEmail = " + bookingEmail)
                .add("passengers = " + passengers)
                .add("phoneNumber = " + phoneNumber)
                .toString();
    }
}
