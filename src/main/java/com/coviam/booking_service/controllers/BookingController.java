package com.coviam.booking_service.controllers;


import com.coviam.booking_service.dto.BookingDTO;
import com.coviam.booking_service.dto.PaymentStatusDTO;
import com.coviam.booking_service.entity.Booking;
import com.coviam.booking_service.service.BookingService;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/booking", produces = APPLICATION_JSON_VALUE)
public class BookingController {

    @Autowired
    public BookingService bookingService;

    private ObjectNode objectNode = JsonNodeFactory.instance.objectNode();


    @PostMapping(
            value = "",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> createBooking(@RequestBody Booking booking) {
        Booking newBooking = bookingService.createBooking(booking);
        return new ResponseEntity<>(newBooking, OK);
    }


    /**
     * GET BOOKING FOR A PARTICULAR BOOKING ID
     *
     * @param id : BOOKING ID
     * @return : BOOKING DTO
     */
    @GetMapping(
            value = "/{id}",
            produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> getBooking(@PathVariable("id") String id) {
        return new ResponseEntity<>(bookingService.getBookingFromId(id), OK);
    }

    @DeleteMapping(
            value = "/{id}"
    )
    public ResponseEntity<?> deleteBooking(@PathVariable("id") String bookingId) {
        bookingService.delete(bookingId);
        objectNode.removeAll();
        objectNode.put("message", "Deleted " + bookingId + " successfully.");
        return ResponseEntity.ok(objectNode);
    }


    /**
     * UPDATE STATUS OF PAYMENT IN A BOOKING, can set it to PENDING, SUCCESSFUL, DEFERRED, CANCELLED
     *
     * @param bookingId        : BOOKING ID
     * @param paymentStatusDTO : JSON OBJECT CONTAINING PAYMENT ID AND PAYMENT STATUS(ENUM VALUES)
     * @return : paymentDTO
     */
    @PutMapping(
            value = "/{id}",
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> updatePaymentStatus(@PathVariable("id") String bookingId,
                                                 @RequestBody PaymentStatusDTO paymentStatusDTO) {
        paymentStatusDTO = bookingService.updateBookingPaymentStatus(bookingId, paymentStatusDTO);
        return new ResponseEntity<>(paymentStatusDTO, OK);
    }

    @GetMapping(
            value = "/all"
    )
    public ResponseEntity<?> getAll() {
        List<BookingDTO> bookingDTOList = bookingService.getAllBookings();
        return ResponseEntity.ok(bookingDTOList);
    }

    @DeleteMapping(
            value = "/all"
    )
    public ResponseEntity<?> deleteAll() {
        bookingService.deleteAll();
        objectNode.removeAll();
        objectNode.put("message", "Deleted all bookings");
        return ResponseEntity.ok(objectNode);
    }

    @GetMapping(
            value = "passenger/all"
    )
    public ResponseEntity<?> getAllPassengers(){
        return ResponseEntity.ok(bookingService.getAllPassengers());
    }

}
