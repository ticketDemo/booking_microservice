package com.coviam.booking_service.repository;

import com.coviam.booking_service.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking,String> {

    @Transactional
    @Modifying
    @Query(value = "update BookingMaster set paymentStatus=:paymentStatus, bookingStatus=:bookingStatus, paymentId=:paymentId where id=:id")
    int setUpdateStatusForBookingId(@Param("id") String bookingId,
                                     @Param("paymentStatus") Booking.Status paymentStatus,
                                     @Param("bookingStatus") Booking.Status bookingStatus,
                                     @Param("paymentId") String paymentId);

    List<Booking> findAll();
}
