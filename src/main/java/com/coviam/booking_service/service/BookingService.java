package com.coviam.booking_service.service;

import com.coviam.booking_service.dto.PaymentStatusDTO;
import com.coviam.booking_service.entity.Booking;
import com.coviam.booking_service.dto.BookingDTO;
import com.coviam.booking_service.entity.Passenger;

import java.util.List;

public interface BookingService {

    /**
     * GET A SINGLE BOOKING
     * @param id : ID OF THE BOOKING
     * @return : BOOKING OBJECT
     */
    BookingDTO getBookingFromId(String id);

    /**
     * INSERT A NEW BOOKING IN BOOKING TABLE
     * @param booking : DATA RECEIVED FROM FRONT END TO BE INSERTED
     * @return : THE DTO THAT WAS PASSED
     */
    Booking createBooking(Booking booking);

    /**
     * UPDATE THE STATUS OF BOOKING THAT IS REFERENCED BY BOOKING ID
     * @param bookingId : ID OF THE BOOKING
     * @param paymentStatusDTO : DTO RECEIVED FROM CALLER
     * @return : THE SAME DTO OBJECT THAT WAS PASSED
     */
    PaymentStatusDTO updateBookingPaymentStatus(String bookingId, PaymentStatusDTO paymentStatusDTO);

    void delete(String bookingId);

    List<BookingDTO> getAllBookings();

    void deleteAll();

    List<Passenger> getAllPassengers();

}
