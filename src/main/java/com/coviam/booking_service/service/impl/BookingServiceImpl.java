//package com.coviam.booking_service.service.impl;
//
//import com.coviam.booking_service.entity.Booking;
//import com.coviam.booking_service.entity.Passenger;
//import com.coviam.booking_service.repository.BookingRepository;
//import com.coviam.booking_service.repository.BookingPassengerRepository;
//import com.coviam.booking_service.dto.BookingDTO;
//import com.coviam.booking_service.dto.PaymentStatusDTO;
//import com.coviam.booking_service.dto.UpdateBookingResponse;
//import com.coviam.booking_service.util.RandomGenerator;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.LinkedList;
//import java.util.List;
//
//@Service
//public class BookingServiceImpl implements com.coviam.booking_service.service.BookingService {
//
//    @Autowired
//    BookingRepository bookingRepository;
//
//    @Autowired
//    BookingPassengerRepository bookingPassengerRepository;
//
//    @Autowired
//    RandomGenerator randomGenerator;

//    @Override
//    public Booking getBookingFromId(String id){
//        return bookingRepository.findOne(id);
//    }
//
//    @Override
//    public Booking createBooking(BookingDTO request){
//
//        Booking newBooking = new Booking(request.getUserId(),
//                randomGenerator.generateRandomString(),
//                request.getSuperPnr(),
//                Booking.Status.PENDING,
//                "",
//                Booking.Status.PENDING,
//                request.getBookingType(),
//                request.getBookingSource(),
//                request.getPhoneNumber(),
//                request.getBookingEmail());
//
//        Passenger bookingPassenger = new Passenger(request.getTravellerDetail().get(0).getName(),request.getTravellerDetail().get(0).getAge());
//        List<Passenger> bookingPassengerList =new LinkedList<Passenger>();
//        bookingPassengerList.add(bookingPassenger);
//        newBooking.setPassengers(bookingPassengerList);
//        Booking newBookingResponse = bookingRepository.save(newBooking);
//        return newBookingResponse;
//    }
//
//    @Override
//    public UpdateBookingResponse updateBookingPaymentStatus(PaymentStatusDTO request){
//        Booking current_booking = this.getBookingFromId(request.getBooking_reference());
//
//        current_booking.setBookingStatus(Booking.Status.SUCCESSFUL);
//        current_booking.setPaymentStatus(Booking.Status.SUCCESSFUL);
//        current_booking.setPaymentId(request.getPayment_id());
//
//        Booking saved_booking = bookingRepository.save(current_booking);
//
//        UpdateBookingResponse response;
//        if(saved_booking != null){
//             response = new UpdateBookingResponse(true,200);
//        }else{
//             response = new UpdateBookingResponse(false,201,"Update Booking Failed");
//        }
//        return response;
//    }
//
//    @Override
//    public UpdateBookingResponse updateBookingPaymentError(PaymentStatusDTO request){
//        Booking current_booking = this.getBookingFromId(request.getBooking_reference());
//
//        current_booking.setBookingStatus(Booking.Status.DEFERRED);
//        current_booking.setPaymentStatus(Booking.Status.DEFERRED);
//        current_booking.setPaymentId(request.getPayment_id());
//
//        Booking saved_booking = bookingRepository.save(current_booking);
//
//        UpdateBookingResponse response;
//        if(saved_booking != null){
//            response = new UpdateBookingResponse(true,200);
//        }else{
//            response = new UpdateBookingResponse(false,201,"Update Booking Failed");
//        }
//        return response;
//    }
//
//    @Override
//    public UpdateBookingResponse updateBookingPaymentCancel(PaymentStatusDTO request){
//        Booking current_booking = this.getBookingFromId(request.getBooking_reference());
//
//        current_booking.setBookingStatus(Booking.Status.CANCELLED);
//        current_booking.setPaymentStatus(Booking.Status.CANCELLED);
//        current_booking.setPaymentId(request.getPayment_id());
//
//        Booking saved_booking = bookingRepository.save(current_booking);
//
//        UpdateBookingResponse response;
//        if(saved_booking != null){
//            response = new UpdateBookingResponse(true,200);
//        }else{
//            response = new UpdateBookingResponse(false,201,"Update Booking Failed");
//        }
//        return response;
//    }
//}
