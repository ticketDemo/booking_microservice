package com.coviam.booking_service.service.impl;

import com.coviam.booking_service.dto.BookingDTO;
import com.coviam.booking_service.dto.PaymentStatusDTO;
import com.coviam.booking_service.entity.Booking;
import com.coviam.booking_service.entity.Passenger;
import com.coviam.booking_service.repository.BookingRepository;
import com.coviam.booking_service.repository.PassengerRepository;
import com.coviam.booking_service.service.BookingService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class BookingServiceImpl2 implements BookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    PassengerRepository passengerRepository;

    @Autowired
    Logger log;

    /**
     * GET A SINGLE BOOKING
     *
     * @param id : ID OF THE BOOKING
     * @return : BOOKING OBJECT
     */
    @Override
    public BookingDTO getBookingFromId(String id) {
        Booking booking = bookingRepository.findOne(id);
        BookingDTO bookingDTO = new BookingDTO();
        log.debug("for id : " + id + " : " + booking);
        return bookingDTO.inflateFromBooking(booking);
    }

    /**
     * Insert a new Booking in `Booking` table
     *
     * @param booking : Data received from front end to be inserted.
     * @return : The object as it is stored in database.
     */
    @Override
    public Booking createBooking(Booking booking) {
        Booking booking1 = bookingRepository.save(booking);
        log.debug("inserted : " + booking1);
        return booking1;
    }

    /**
     * UPDATE THE STATUS OF BOOKING THAT IS REFERENCED BY BOOKING ID
     *
     * @param bookingId        : ID OF THE BOOKING
     * @param paymentStatusDTO : DTO RECEIVED FROM CALLER
     * @return : THE SAME DTO OBJECT THAT WAS PASSED
     */
    @Override
    public PaymentStatusDTO updateBookingPaymentStatus(String bookingId, PaymentStatusDTO paymentStatusDTO) {
        paymentStatusDTO.setBookingId(bookingId);
        log.debug("payment status dto : " + paymentStatusDTO);
        int linesUpdated = bookingRepository.setUpdateStatusForBookingId(bookingId,
                Booking.Status.valueOf(paymentStatusDTO.getStatus()),
                Booking.Status.valueOf(paymentStatusDTO.getStatus()),
                paymentStatusDTO.getPaymentId());

        log.debug(linesUpdated + " lines updated in db");
        return paymentStatusDTO;
    }

    @Override
    public void delete(String bookingId) {
        bookingRepository.delete(bookingId);
    }

    @Override
    public List<BookingDTO> getAllBookings() {
        List<Booking> bookingList = bookingRepository.findAll();
        List<BookingDTO> bookingDTOList = new ArrayList<>();
        bookingList.forEach(booking -> bookingDTOList.add(new BookingDTO().inflateFromBooking(booking)));
        return bookingDTOList;
    }

    @Override
    public void deleteAll() {
        bookingRepository.deleteAll();
    }

    @Override
    public List<Passenger> getAllPassengers() {
        List<Passenger> passengerList = passengerRepository.findAll();
        log.debug(passengerList.get(0));
        return passengerList;
    }


}
